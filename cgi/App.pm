package App;

use base 'CGI::Application';

use strict;
use warnings;
use utf8;

use CGI::Application::Plugin::Session;
use CGI::Application::Plugin::TT;
use CGI::Application::Plugin::DBH qw(dbh_config dbh);

use Encode;

use Users;

sub setup {
    my ($self) = @_;
    $self->header_props(-type => 'text/html;charset=utf8');
    $self->start_mode('login');
    $self->run_modes(
        'login' => 'login_page',
        'logout' => 'logout_page',
        'users' => 'users_page'
    );
}

sub cgiapp_init {
    my ($self) = @_;

    $self->session_config(
        'CGI_SESSION_OPTIONS' => [
            'driver:file',
            $self->query(),
            { 'Directory' => '/opt/cp/sessions' }
        ],
        'COOKIE_PARAMS' => {
            -path => '/'
        },
        'SEND_COOKIE' => 1
    );

    $self->tt_config(
        'TEMPLATE_OPTIONS' => {
            'INCLUDE_PATH' => '/opt/cp/tmpl',
        },
    );

    $self->dbh_config('dbi:Pg:database=cp;host=localhost', 'cp', '');

    $self->{'Users'} = Users->new('dbh' => $self->dbh());
}

sub cgiapp_prerun {
    my ($self) = @_;

    my $q = $self->query();

    if (!$self->session->param('logged')) {
        $self->prerun_mode('login');
    }
    elsif (!$q->param('rm')) {
        $self->prerun_mode('users');
    }
}

sub users_page {
    my ($self) = @_;

    my $q = $self->query();
    my %data = ();
    map { $data{ $_ } = Encode::decode('utf8', $q->param($_)) } qw(sort);

    my $output;

    foreach my $ref (
        $self->getNavBarHTML(),
        $self->getUsersHTML('sort' => $data{'sort'})
    ){
        if ($ref) {
            $output .= $$ref;
        }
    }

    return $self->pageOutput(\$output);
}

sub login_page {
    my ($self) = @_;

    my $q = $self->query();
    my %formData = ();
    map { $formData{ $_ } = Encode::decode('utf8', $q->param($_)) } qw(username email submit);

    my $error = undef;

    if ($formData{'submit'}) {
        my $formValidationError = $self->validateForm([
            ['username', qr/^[a-zA-Zа-яА-Я]+$/],
            ['email', qr/^[^\@]+\@[^\@]+?\.\w{2,}$/]
        ]);

        if (!$formValidationError){
            my $user = $self->{'Users'}->get(
                'name' => $formData{'username'},
                'email' => $formData{'email'}
            ) || $self->{'Users'}->add(
                'name' => $formData{'username'},
                'email' => $formData{'email'}
            );

            if (!$user) {
                $error = 'Cannot create user';
            }
        }
        else {
            $error = $formValidationError;
        }
    }

    my $output = '';

    if ($formData{'submit'} && !$error) {
        $self->session->param('logged' => 1);
        $self->header_type('redirect');
        $self->header_props(-url => '/cgi-bin/app.pl');
    }
    else {
        my $ref = $self->getLoginFormHTML(\%formData, $error);
        $output = $ref ? $$ref : '';
    }

    return $self->pageOutput(\$output);
}

sub logout_page {
    my ($self) = @_;
    $self->session->clear('logged');
    $self->header_type('redirect');
    $self->header_props(-url => '/cgi-bin/app.pl');
}

sub pageOutput {
    my ($self, $outputRef) = @_;

    my $q = $self->query();

    return \join("\n",
        $q->start_html(-title => 'Example web app'),
        $$outputRef,
        $q->end_html()
    );
}

sub getNavBarHTML {
    my ($self) = @_;
    return $self->tt_process('navbar.html');
}

sub getUsersHTML {
    my ($self, %params) = @_;

    my $users = $self->{'Users'}->list('sort' => $params{'sort'});

    return $self->tt_process('users.html', {
        'users' => $users
    });
}

sub getLoginFormHTML {
    my ($self, $dataRef, $error) = @_;
    my $q = $self->query();

    return $self->tt_process('login.html', {
        'username' => $dataRef->{'username'},
        'email' => $dataRef->{'email'},
        'error' => $error
    });
}

sub validateForm {
    my ($self, $optsRef) = @_;

    my $q = $self->query();
    my $error = undef;

    if ($optsRef
        && (ref($optsRef) eq 'ARRAY')
    ){
        my @errors = ();
        for my $opt (@$optsRef) {
            if ($opt
                && (ref($opt) eq 'ARRAY')
                && $opt->[0]
            ){
                my ($field, $re) = @$opt;
                my $value = $q->param($field);

                if (!$value
                    || ($re
                        && ($value !~ /$re/)
                    )
                ){
                    push @errors, $field;
                }
            }
        }
        if (@errors) {
            $error = 'Invalid fields: '.join(', ', @errors);
        }
    }

    return $error;
}

1;
