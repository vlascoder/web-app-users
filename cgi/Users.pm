package Users;

use strict;
use warnings;
use utf8;

sub new {
    my ($class, %params) = @_;

    return if !$params{'dbh'};

    my $self = {
        'dbh' => $params{'dbh'}
    };

    bless $self, $class;

    return $self;
}

sub list {
    my ($self, %params) = @_;

    my $sort = (grep { $params{'sort'} eq $_ } qw(name email))[0] || 'created';

    my $users = $self->{'dbh'}->selectall_arrayref(
        "SELECT * FROM users ORDER BY $sort",
        {'Slice'=>{}}
    );

    return $users;
}

sub get {
    my ($self, %params) = @_;

    return if !$params{'name'} || !$params{'email'};

    return $self->{'dbh'}->selectrow_hashref(
        "SELECT * FROM users WHERE name = ? AND email = ?",
        undef,
        $params{'name'},
        $params{'email'}
    );
}

sub add {
    my ($self, %params) = @_;

    return if !$params{'name'} || !$params{'email'};

    return $self->{'dbh'}->do(
        "INSERT INTO users (name,email,created) VALUES (?,?,NOW())",
        undef,
        $params{'name'},
        $params{'email'}
    );
}

1;
