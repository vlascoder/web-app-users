#!/usr/bin/perl

use strict;
use warnings;
use utf8;

use lib '.';
use App;

my $app = App->new();
$app->run();

