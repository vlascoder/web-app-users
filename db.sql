CREATE TABLE users (
    name VARCHAR(100),
    email VARCHAR(100),
    created TIMESTAMP WITHOUT TIME ZONE,
    UNIQUE(name, email)
);
